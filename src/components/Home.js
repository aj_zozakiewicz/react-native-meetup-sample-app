import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView
} from 'react-native';

import * as colors from '../styles/colors';
import globalStyles from '../styles/globalstyles';

import Card from './Card';
import Movies from './Movies';
import NavBar from 'react-native-tab-bar';

const Home = ({movies}) => (

    <View style={globalStyles.appWrapper}>
        <Text style={styles.appTitle}>My Movie App</Text>

        <NavBar contentStyle={globalStyles.tabWrapper}
                selectedColor={colors.PRIMARY_BLUE}
                unselectedColor={colors.LIGHT_GREY}
        >
            <Movies movies={movies} icon="account-circle"/>
            <ScrollView icon="announcement">
                <Text>Another Tab</Text>
            </ScrollView>
            <ScrollView icon="settings">
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 4}}>
                        <Text style={styles.appTitle}> I am 4/10 of the row. </Text>
                    </View>

                    <View style={{flex: 6}}>
                        <Text style={styles.appTitle}> I am 6/10 of the row. </Text>
                    </View>
                </View>
            </ScrollView>
        </NavBar>
    </View>
);

const styles = StyleSheet.create({
    appTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily: 'OpenSans-CondensedBold',
        marginTop: 40,
        paddingLeft: 20,
        paddingRight: 20
    }
});

export default Home;
