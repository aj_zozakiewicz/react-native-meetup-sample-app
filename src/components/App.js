/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';

import * as Actions from '../store/actions/actions';
import routes from '../constants/navitems';
import Login from './Login';
import Home from './Home';

import {
    View,
    ScrollView,
    Text
} from 'react-native';

class App extends Component {
    render() {
        const {
            movies,
            route,
            routeParams,
            AttemptLogin
        } = this.props;

        switch(route) {
            case routes.LOGIN:
                return <Login data={routeParams} login={AttemptLogin}/>;
            case routes.HOME:
                return <Home movies={movies}/>;
            default:
                return null;
        }
    }
}

function mapStateToProps(state) {
    return {
        movies: state.mainReducer.movies,
        route: state.navReducer.current,
        routeParams: state.navReducer.routeParams
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({...Actions}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
