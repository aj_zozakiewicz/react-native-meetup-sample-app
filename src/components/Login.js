import React, { Component } from 'react';
import TouchID from 'react-native-touch-id';

import * as colors from './../styles/colors';
import globalstyles from './../styles/globalstyles';

import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    View
} from 'react-native';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            supportsTouchId: false,
            checkingTouchID: true,
            pass: '',
            error: ''
        };
    }

    checkTouchId() {
        console.log(TouchID.isSupported);

        TouchID.isSupported()
            .then(supported => {
                this.setState({
                    supportsTouchId: true,
                    checkingTouchID: false
                });
            })
            .catch(error => {

                console.log('not supported', error);

                this.setState({
                    checkingTouchID: false
                });
            });
    }

    promptForTouch(){
        //TOUCH ID is Supported
        TouchID.authenticate()
            .then((success) => {
                this.props.login();
            })
            .catch((error) => {
                console.log('Error with touch id' + error);
                this.setState({
                    supportsTouchId: false
                });
            });
    }

    componentDidMount() {
        this.checkTouchId();
    }

    render() {
        if (!this.state.checkingTouchID) {
            if (this.state.supportsTouchId) {
                this.promptForTouch();
                return (
                    <View style={{flex: 1}}>
                        <Text>ReactNativeSampleApp</Text>
                    </View>
                )
            }

            return (
                <View style={[globalstyles.appWrapper, styles.wrapper]}>
                    <TextInput placeholder="Password"
                               secureTextEntry={true}
                               autoCapitalize={'none'}
                               clearButtonMode="while-editing"
                               onChange={(event) => this.setState({
                                   pass: event.nativeEvent.text
                               })}
                               style={globalstyles.textInput}
                    />

                    { this.props.params && this.props.params.error ? <Text style={styles.error}>{this.props.params.error}</Text> : null }

                    <TouchableOpacity
                        style={globalstyles.buttonPrimary}
                        onPress={() => {
                            this.props.login(false, this.state.pass);
                        }}
                    >
                        <Text>Login</Text>
                    </TouchableOpacity>
                </View>
            )
        }

        return null;
    }

}

const styles = StyleSheet.create({
    error: {
        color: colors.RED
    },
    wrapper: {
        paddingTop: 40,
        paddingLeft: 20,
        paddingRight: 20
    }
});

export default Login;
