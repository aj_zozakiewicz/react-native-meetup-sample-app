const ENV = 'prod';

const config = () => {

    switch(ENV) {
        case 'dev':
            return {
                movies: 'http://facebook.github.io/react-native/movies.json'
            };
        case 'demo':
            return {
                movies: 'http://facebook.github.io/react-native/movies.json'
            };
        case 'prod':
            return {
                movies: 'http://facebook.github.io/react-native/movies.json'
            };
        default:
            return {};
    }
};

export default config();
