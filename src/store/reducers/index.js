import {combineReducers} from 'redux';
import mainReducer from './main';
import navReducer from './navreducer';

const rootReducer = combineReducers({
  mainReducer,
  navReducer
});

export default rootReducer
