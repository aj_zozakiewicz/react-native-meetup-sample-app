import routes from './../../constants/navitems';
import actions from './../actions/actiontypes';
import _ from 'lodash';

const initialState = {
    previous: routes.LOGIN,
    current: routes.LOGIN,
    authenticated: false,
    routeParams: null
};

export default function navReducer(state = initialState, { type, payload }) {
    switch(type) {
        case actions.NAVIGATE:
            return _.assign({}, state, {
                previous: state.current,
                current: payload.route,
                routeParams: payload.params
            });
        case actions.LOGIN_SUCCESS:
            return _.assign({}, state, {
                authenticated: true
            });
        default:
            return state;
    }
}
