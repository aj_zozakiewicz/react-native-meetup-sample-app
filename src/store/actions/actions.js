import ActionTypes from './actiontypes';
import * as helpers from './helpers';
import config from './../../utils/config';
import routes from './../../constants/navitems';

export function fetchMovies() {
    return (dispatch) => {
        return fetch(config.movies).then((response) => {
            if(response.status !== 200){
                throw new Error('error', response.statusText);
            }
            return response.json();
        })
        .then((json) => {
            console.log(json);

            return dispatch(helpers.successAction(ActionTypes.API_SUCCESS, json))
        })
        .catch((error) => {
            console.log(error);
            dispatch(helpers.failureAction('ERROR', error))
        });
    };
}

export function AttemptLogin(touchIdAuth, password) {
    return (dispatch) => {
        if(touchIdAuth || password === '') {
            dispatch(helpers.successAction(ActionTypes.LOGIN_SUCCESS, true));
            return dispatch(Navigate(routes.HOME))
        }

        dispatch(helpers.successAction(ActionTypes.LOGIN_ERROR, 'Error logging in. try again'));
        return dispatch(Navigate(routes.LOGIN, {
            error: 'invalid password'
        }));
    }
}

export function Navigate(route, params = null) {
    return (dispatch) => {
        dispatch(helpers.successAction(ActionTypes.NAVIGATE, {
            route,
            params
        }));
    };
}

export function fetchInitialState() {
    return (dispatch) => {
        dispatch(fetchMovies());
    };
}
