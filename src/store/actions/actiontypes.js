import keyMirror from 'keymirror';

const ActionTypes = keyMirror({
    CALL_API: null,
    LOGIN_SUCCESS: null,
    LOGIN_ERROR: null,
    API_SUCCESS: null,
    NAVIGATE: null
});

export default ActionTypes;
