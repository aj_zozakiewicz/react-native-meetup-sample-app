import keyMirror from 'keymirror';

const NavActionTypes = keyMirror({
    LOGIN: null,
    HOME: null
});

export default NavActionTypes;
