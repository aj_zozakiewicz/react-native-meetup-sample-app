import { StyleSheet } from 'react-native';
import * as colors from './colors';

const standardBorderWidth = 1;

const globalstyles = StyleSheet.create({
    appWrapper: {
        backgroundColor: colors.OFF_WHITE,
        flex: 1
    },
    tabWrapper: {
        padding: 20,
        flex: 1
    },
    columnWrap: {
        flexDirection: 'row'
    },
    textInput: {
        height: 40,
        backgroundColor: colors.WHITE,
        borderColor: colors.LIGHT_GREY,
        borderWidth: 1,
        marginBottom: 10,
        paddingLeft: 10
    },
    buttonPrimary: {
        borderColor: colors.CHARCOAL,
        borderWidth: standardBorderWidth,
        borderRadius: 5,
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    }
});

export default globalstyles;
